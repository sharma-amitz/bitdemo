/*
 *
 * 1.This code reads data from snowplow_events_raw and finds aid , event_count , dt for each aid. Joins with data in snowplow_products to get application name
 * 2.Computes event_count_avg for each aid for last 7 days and joins with point # 1 (snowplow_events_raw join snowplow_products)
 * 3.Finally populates table app_eventcount with aid ,application , event_count ,event_count_avg,dt.
 *
 */

package com.clarivate.snowplowAppEventCount

import org.joda.time.DateTime
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.SaveMode

case class SPRaw(aid: String, dt: String, event_count: Long)
case class SPProducts(aid: String, application: String)
case class SPWithApp(aid: String, dt: String, event_count: Long, application: String)
case class SPEventAvg(aid: String, event_count_avg: Double)
case class SPFinalTable(aid: String, application: String, event_count: Long, event_count_avg: Double, dt: String)

/*
 * 
 * For testing purpose only 
 * 
 */

object AidEventCount {

  def enrichmentPlusWriteToTable(spark: SparkSession, inputTable: String, snowplowProducts: String, startDate: String, startDate7old: String, enrichedTable: String) = {

    import spark.implicits._

    val spProductsDF = spark.sql(s"select aid , application from $snowplowProducts").as[SPProducts].cache()
    val broadcastedDF = broadcast(spProductsDF)

    //Present day calculations:
    val spRawDF = spark.sql(s"select aid, dt , count(*) as event_count from $inputTable where dt ='$startDate' group by aid , dt ").as[SPRaw].cache()
    val spWithApplicationDF = spRawDF.join(broadcastedDF, broadcastedDF.col("aid").equalTo(spRawDF.col("aid"))).drop(broadcastedDF.col("aid")).as[SPWithApp].cache()

    //Last 7 days calculations below:
    val spRaw7DF = spark.sql(s"select aid , dt , count(*) as event_count from $inputTable where dt >'$startDate7old' and dt < '$startDate' group by aid , dt ").as[SPRaw].cache()
    val spWithApplication7DF = spRaw7DF.join(broadcastedDF, broadcastedDF.col("aid").equalTo(spRaw7DF.col("aid"))).drop(broadcastedDF.col("aid")).as[SPWithApp].cache()
    val spWithAvgEventCountDF = spWithApplication7DF.groupBy("aid").agg(avg("event_count").as("event_count_avg")).as[SPEventAvg].cache()

    //Final join calculations: (joining on aid to get event_count_avg and event_count/day for each aid )
    val finalDF = spWithApplicationDF.join(spWithAvgEventCountDF, spWithApplicationDF.col("aid").equalTo(spWithAvgEventCountDF.col("aid"))).drop(spWithAvgEventCountDF.col("aid"))
      .select(col("aid"), col("application"), col("event_count"), col("event_count_avg"), col("dt")).as[SPFinalTable].cache()

    //writing to the tabe
    finalDF.coalesce(1).write.mode(SaveMode.Overwrite).insertInto(s"$enrichedTable")
    spark.sql(s"msck repair table $enrichedTable ")

  }

  def main(args: Array[String]) {

    val spark = SparkSession.builder().appName("aidEventCount").enableHiveSupport().getOrCreate()

    val topenv = args(0) //prod
    val subenv = args(1) //prod1
    val database = "neon_" + topenv + "_" + subenv //neon_prod_prod1

    val inputTable = args(2) // "snowplow_events_raw"
    val snowplowProducts = args(3) //  "snowplow_products"
    val startDate = args(4) //"2020-05-30"
    val startDate7old = new DateTime(DateTime.parse(startDate)).minusDays(8).toString("YYYY-MM-dd")
    val enrichedTable = args(5) //"default.app_eventcount"

    spark.sql(s"USE $database")
    spark.sql("set hive.exec.dynamic.partition.mode=nonstrict")

    enrichmentPlusWriteToTable(spark, inputTable, snowplowProducts, startDate, startDate7old, enrichedTable)

  }

}
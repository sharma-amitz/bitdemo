package com.clarivate.code

import org.apache.spark.sql.SparkSession

object RDDwithCSvFiles {

  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()

    val csvRDD = sparkSession.sparkContext.textFile("src/main/resources/Datasets/2016-stack-overflow-survey-responses.csv")

    println(csvRDD.count)

    csvRDD.take(10).foreach(println)

    val header = csvRDD.first 
    val csvWithorutHeader = csvRDD.filter(_ != header)

    csvWithorutHeader.take(10).foreach(println)

    val limitedColumns = csvWithorutHeader.map(line => {
      val colArray = line.split(",")
      Array(colArray(0), colArray(1), colArray(2)).mkString(",")
    })

    limitedColumns.take(10).foreach(println)
    
    limitedColumns.saveAsTextFile("output/limitedColumns")
    //csvWithorutHeader.take(10).foreach(println)
    
    

  }
}
package com.clarivate.code

import org.apache.spark.sql.SparkSession

object RDDSaprkSession {

  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()
    val array = Array(1,2,3,4,5)
    val arrayRDD = sparkSession.sparkContext.parallelize(array, 3)
    println("Size of the array is " + arrayRDD.count)
    arrayRDD.foreach(println)
    
    val file ="C:\\Users\\U6026215\\Desktop\\in\\airports.txt";
    
    val fileRDD = sparkSession.sparkContext.textFile(file)
    
    println(fileRDD.count())
    println("contents of the file are "+fileRDD.take(10).foreach(println))
    
  }

}
package com.clarivate.code

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.functions._
//import org.apache.spark.sql.functions.coalesce
//import org.apache.spark.sql.catalyst.expressions.aggregate.Average
import org.apache.spark.sql.expressions.Window

case class realestate(MLS: Integer, Location: String, Price: Integer, Bedrooms: Integer, Size: Integer, Price_SQ_Ft: Double, Status: String)
object CreateOwnSchema {

 // case class realestate(MLS: Integer, Location: String, Price: Integer, Bedrooms: Integer, Size: Integer, Price_SQ_Ft: Double, Status: String)

  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()

    val baseDF = sparkSession.read.option("header", true).option("inferSchema", true).csv("src/main/resources/Datasets/realestate.csv")

    baseDF.printSchema()

    //baseDF.show(35,false)

    val schema = StructType(
      StructField("MLS", IntegerType, true) ::
        StructField("Location", StringType, true) ::
        StructField("Price", IntegerType, true) ::
        StructField("Bedrooms", IntegerType, true) ::
        StructField("Bathrooms", IntegerType, true) ::
        StructField("Size", IntegerType, true) ::
        StructField("Price_SQ_Ft", DoubleType, true) ::
        StructField("Status", StringType, true) :: Nil)

    import sparkSession.implicits._
    
    // we can create case class inside the object , however the visibility will only be inside object , however when we create a case class outside the object 
    // will be visible in the entire package. 
    
    val baseDFOwnSchema = sparkSession.read.option("header", true).schema(schema).csv("src/main/resources/Datasets/realestate.csv").as[realestate]

    baseDFOwnSchema.printSchema();

    // Use of filter and where clauses in the Dataset 
    
    var filteredbaseDFOwnSchema = baseDFOwnSchema.filter( RealEstataobj =>  RealEstataobj.Location=="King City")
    //var filteredbaseDFOwnSchema = baseDFOwnSchema.where(col("Location") === "King City")
    filteredbaseDFOwnSchema.show(35,false)
    
    var baseDataset = sparkSession.read.option("header", true).schema(schema).csv("src/main/resources/Datasets/realestate.csv")

    // filetering of the rows based on location
    //baseDataset = baseDataset.select(col("MLS"), col("Location"),col("Price")).filter(col("Location").equalTo("Santa Maria-Orcutt"))

    //baseDataset = baseDataset.select(col("MLS"), col("Location"),col("Price")).filter(col("Location").no
    
    //baseDataset.show(35, false)

    // Grouping to find average of the price
    var baseDatasetGroupByLocation = baseDFOwnSchema.select(col("MLS"), col("Location"), col("Price")).groupBy(col("Location")).avg("Price").as("avg_price") //.filter(col("Location").equalTo("Pismo Beach"))
    baseDatasetGroupByLocation.show(100, false)

    //doing two aggregation (Sum , avg )  on price using groupBy clause

    var test = baseDFOwnSchema.select(col("MLS"), col("Location"), col("Price")).groupBy(col("Location")).agg(avg(col("Price")), sum(col("Price"))) //.avg("Price").as("avg_price")
    test.show(100, false)

    //joining avg_price with base dataset
    var finalDeliverDataset = baseDatasetGroupByLocation.join(baseDataset, baseDatasetGroupByLocation.col("Location") === baseDataset.col("Location"), "inner")
      .select(col("MLS"), baseDatasetGroupByLocation.col("Location"), col("Price"), col("avg(Price)").as("avg_price"))
    finalDeliverDataset.show(35, false)

    // one statement to find average of the price , no joining needed at all --- one liner

    var windOverPartitionBy = baseDFOwnSchema.withColumn("avg_price", avg(col("Price")) over Window.partitionBy(col("Location")))

    println("base dataset with avg price below")
    windOverPartitionBy.show(100, false)

    // collecting all the prices for a particular location in  one column using collect_list and average price of that location
    var windOveParttionByCollectList = windOverPartitionBy.withColumn("prices", collect_list(col("Price")) over Window.partitionBy(col("Location")))
    println("base dataset with avg price and avg price  below")
    windOveParttionByCollectList.show(100, false)

    //var CostliestLocationWithAvgPrice = windOveParttionByCollectList.select(col("*") , max(col("Price")) over Window.partitionBy(col("Location")))

    //Finding Max price per location and adding that to previous dataset "windOveParttionByCollectList"
    var CostliestLocationWithAvgPrice = windOveParttionByCollectList.select(col("*")).withColumn("Max_Price", max(col("Price")) over Window.partitionBy(col("Location"))) // , max(col("Price")) over Window.partitionBy(col("Location")))

    //println("look below if same ")
    CostliestLocationWithAvgPrice.show(100, false)

  }
}
package com.clarivate.code

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions.col

object UDFCurry {

  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder().appName("Spark UDF with CUrry functions").master("local").getOrCreate()

    val properties = Map("header" -> "true", "inferSchema" -> "true")

    val df = sparkSession.read.options(properties).csv(args(0))

    df.show(35, false)

    //This is read from the argument provide at the input to add up to the column Location . This is Curry Function way of creating a UDF .
    // In normal UDF we cant proceed with additonal value other then columns in DatatSet or Dataframe , But in CUrry function way we can do it .
    // Here Yo is passed as addToLocation to be added to the Location .
    val addToLocation = args(1).toString()

    val newdf = df.withColumn("LocationNew", toUpper(addToLocation)(df.col("Location")))
    newdf.show(35, false)

  }

  def toUpper(addToLocation: String): UserDefinedFunction = udf((Location: String) => {

    Location.toUpperCase() + " " + addToLocation
  })

}
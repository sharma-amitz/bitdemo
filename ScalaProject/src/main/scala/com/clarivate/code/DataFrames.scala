package com.clarivate.code

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.IntegerType

object DataFrames {
  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()
    val rdd = sparkSession.sparkContext.parallelize(Array(1, 2, 3, 4, 5))

    val schema = StructType(
      StructField("Array_Columns", IntegerType, true) :: Nil)

    val rowRDD = rdd.map(element => Row(element))
    val df = sparkSession.createDataFrame(rowRDD, schema)
    df.printSchema()
    df.show(35, false)
  }
}
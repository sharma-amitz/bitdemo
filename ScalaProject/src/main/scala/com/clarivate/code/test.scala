package com.clarivate.code

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
object test {

  def main(args: Array[String]) {

    val spark = SparkSession.builder().appName("Spark Session RDD basics").enableHiveSupport().getOrCreate()
    val steamCortellis_extractDF = spark.sql(s"select a , j from default.steam_cortellis_extract")

    val snowplow_events_cortellisDF = spark.sql(s"select * from neon_prod_prod1.snowplow_events where dt='2020-04-17' and aid ='lsc' or aid ='cortellis'")

    val cortellis_steam_finalDF = steamCortellis_extractDF.join(snowplow_events_cortellisDF, col("a").equalTo(col("uid"))).drop("customer_name").withColumnRenamed("j", "customer_name")

    cortellis_steam_finalDF.write.option("header", true).csv("s3://1p-data-snowplow-prod/amit_test/cortellis/dataToCheck/")

    //val testdf = spark.sql(s"select uid, is_internal from neon_prod_prod1.neon_user_meta limit 10")

    //testdf.show(35,false)

  }
}
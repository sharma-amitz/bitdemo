package com.clarivate.code

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object RddBasics {

  def main(args: Array[String]): Unit = {
    val sparkconf = new SparkConf()

    sparkconf.setAppName("FirstRDDCode").setMaster("local")
    val sc = new SparkContext(sparkconf)
    val array = Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    val arrayRDD = sc.parallelize(array)
    println("Number if elments in RDD are " + arrayRDD.count())
    arrayRDD.foreach(println)
    val inlist = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val listRDD = sc.parallelize(inlist)
    println("partition size of list is :" + listRDD.partitions.length)
    println("count of elements are " + listRDD.count())
    listRDD.foreach(println)
  }

}
package com.clarivate.code

import org.apache.spark.sql.SparkSession
import scala.util.matching.Regex

object LetsSee {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()

    val pageDefsTable = spark.read.json("C:\\Users\\U6026215\\Downloads\\pagedefinitions.txt");

    val pageDefsDF = pageDefsTable.select("application", "pageName", "pageValues")
    //println("pageDefsDF has : "+pageDefsDF)
    pageDefsDF.show(35,false)
    val pageDefsRows = pageDefsDF.collect

    
    var results = scala.collection.mutable.Map[String, Array[(String, Regex)]]()
    for (row <- pageDefsRows) {
      val aid = row.getAs[String]("application").toLowerCase
      val pageName = row.getAs[String]("pageName")
      val pageValue = row.getAs[String]("pageValues")
      try {
        val regObj = new Regex(pageValue.replace("+", "\\+"))
        //println("aid : "+aid)
        //println("pageName : "+pageName)
        //println("pageValue : "+pageValue)
        //println("regObj : "+regObj)
        val arr = results.getOrElse(aid, Array[(String, Regex)]()) :+ (pageName, regObj)
        results += (aid -> arr)
        
        // [cadp -> array(Register,.*/register.*),(),(),(),(),(),(),()]
      } catch {
        case e: Exception => println("*Page definitaion is wrong (aid:%s, pageName:%s, pageValue:%s): %s".format(aid, pageName, pageValue, e.getMessage))
      }
    }
   
    
    for ((k,v) <- results) printf("key: %s, value: %s\n", k, v(0))
    
 /*   // For debugging
    println("Page definitions per app:")
    results.foreach { x =>
      println(x._1)
      x._2.foreach( y => println(y))    
      
    }*/
    //println("Results looks like : " + results._2._1)
  }

}
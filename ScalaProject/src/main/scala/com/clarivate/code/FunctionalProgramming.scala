package com.clarivate.code

object FunctionalProgramming extends App {
  
  
  val incrementer:Int=>Int=x=>x+1
  
  
  val incremented = incrementer(42)
  
  println("incremented value is " + incremented)
  
  
}
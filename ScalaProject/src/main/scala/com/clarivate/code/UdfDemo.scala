package com.clarivate.code

import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions._

object UdfDemo extends App {

  val sparkSession = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()

  val schema = StructType(
    StructField("MLS", IntegerType, true) ::
      StructField("Location", StringType, true) ::
      StructField("Price", IntegerType, true) ::
      StructField("Bedrooms", IntegerType, true) ::
      StructField("Bathrooms", IntegerType, true) ::
      StructField("Size", IntegerType, true) ::
      StructField("Price_SQ_Ft", DoubleType, true) ::
      StructField("Status", StringType, true) :: Nil)

  import sparkSession.implicits._

  // we can create case class inside the object , however the visibility will only be inside object , however when we create a case class outside the object
  // will be visible in the entire package.

  var baseDFOwnSchema = sparkSession.read.option("header", true).schema(schema).csv("src/main/resources/Datasets/realestate.csv").as[realestate]

  //2 . convert the function to UDF  =udf[o/p parameter , i/p parameter](function name defined at pointer 1)
  val toUpperUDF = udf[String, String](toUpper)

  //defining case class inside object , now the visibility of the case class with be inside object only
  case class upperLocation(MLS: Integer, Location: String, Price: Integer, Bedrooms: Integer, Size: Integer, Price_SQ_Ft: Double, Status: String, LocationUpper: String)

  //3 Use UDF in the Dataset/Dataframes
  var transformedbaseDFOwnSchema = baseDFOwnSchema.withColumn("LocationUpper", toUpperUDF(col("Location"))).as[upperLocation] //(toUpperUDF(baseDFOwnSchema.col("Location"))).as[upperLocation]

  transformedbaseDFOwnSchema.show(100, false)

  // UDF creation steps .

  // 1.defining  the method
  def toUpper(s: String): String = { s.toUpperCase() }

}
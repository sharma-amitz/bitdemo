package com.clarivate.code

import org.apache.spark.sql.SparkSession

object DataframeWithCsv {

  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder().appName("Spark Session RDD basics").master("local").getOrCreate()

    val properties = Map("header" -> "true", "inferSchema" -> "true")

    //val df = sparkSession.read.option("header", true).option("inferSchema", true).csv("src/main/resources/Datasets/realestate.csv")

    val df = sparkSession.read.options(properties).csv("src/main/resources/Datasets/realestate.csv")

    df.printSchema()
    df.show(35, false)

  }

}
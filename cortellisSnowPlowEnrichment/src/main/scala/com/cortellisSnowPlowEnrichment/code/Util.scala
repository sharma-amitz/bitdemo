package com.cortellisSnowPlowEnrichment.code

import java.text.SimpleDateFormat

import scala.collection.JavaConversions.{ collectionAsScalaIterable => asScala }

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.S3ObjectSummary

object Util extends Serializable {

  def BackFillDates(ecrichBucket:String  , enrichPrefix:String, steamBucket:String, staemPrefix:String) = { //:Map[Set[String], String] = {

    val s3Client = new AmazonS3Client
    val originFormat = new SimpleDateFormat("yyyymmdd")
    val targetFormat = new SimpleDateFormat("yyyy-mm-dd")

    //var set = scala.collection.mutable.Set[String]()
    def map[T](s3: AmazonS3Client, bucket: String, prefix: String)(f: (S3ObjectSummary) => T) = {

      def scan(acc: List[T], listing: ObjectListing): List[T] = {
        val summaries = asScala[S3ObjectSummary](listing.getObjectSummaries())
        val mapped = (for (summary <- summaries) yield f(summary)).toList
        if (!listing.isTruncated) mapped.toList
        else scan(acc ::: mapped, s3.listNextBatchOfObjects(listing))
      }
      scan(List(), s3.listObjects(bucket, prefix))
    }

    val enrichedTableDatesSet = map(s3Client, ecrichBucket, enrichPrefix)(s => s.getKey)
      .filter(s => s.contains("="))
      .map(s => s.split("/")(3))
      .filter(s => !s.contains("$folder$"))
      .distinct.map(s => s.split("=")(1))
      .toSet // Set(2020-04-17, 2020-04-26)

    
      val steamCortellisDatesSet = map(s3Client, steamBucket, staemPrefix)(s => s.getKey)
      .filter(s => !s.contains("$folder$"))
      .map(s => s.split("/")(3))
      .map(s => targetFormat.format(originFormat.parse(s))).dropRight(1)
      .toSet
    
      val lastSteamCortellisDate = map(s3Client, steamBucket, staemPrefix)(s => s.getKey)
      .filter(s => !s.contains("$folder$"))
      .map(s => s.split("/")(3))
      .map(s => targetFormat.format(originFormat.parse(s))).last
      
    var datesToProcess: Set[String] = Set()

    //for bad case when steamsCortellis data is not sent , returning map[ emptySet() -> lastStaemCortellisDate]

    if (enrichedTableDatesSet.diff(steamCortellisDatesSet).size > 0) {
      (datesToProcess, lastSteamCortellisDate)
    } 
    // for good case scenario when we get regular data from steamsCOrtellis
    else {
      datesToProcess = steamCortellisDatesSet.diff(enrichedTableDatesSet)
      //val datesToProcess = steamCortellisExtract.diff(enrichedTableDatesSet) // Set(2020-04-21, 2020-04-27, 2020-04-24, 2020-04-25, 2020-04-28, 2020-04-30, 2020-04-29, 2020-04-22, 2020-04-23)
      (datesToProcess, lastSteamCortellisDate)
    }
    
  }

}
/*
 *
 * This code takes 6 arguments (prod prod1 snowplow_events snowplow_events_cortellis prevDay currDay)
 *
 *
 * Purpose of the code is as under (https://jira.clarivate.io/browse/USERINTEL-443)
 *
 * 1.We read data from s3://steam-user-data/cortellis/latest into table neon_prod_prod1.steam_cortellis_staging. (Current days cortellis extract from STeam) and take distincy of user_id and customer_name
 * 2.Read data from neon_prod_prod1.snowplow_events table where dt>prevDay and dt <=currDay
 * 3.Join 1&2 on (steam_cortellis_staging.user_id)=(snowplow_events.uid) and fetch customer_name from steam_cortellis_staging instead of snowplow_events.
 * Write partitioned data to neon_prod_prod1.snowplow_events_cortellis
 */

package com.cortellisSnowPlowEnrichment.code

import org.apache.spark.sql.SparkSession

import org.apache.spark.sql.functions._
import org.apache.spark.sql.SaveMode
import org.joda.time.DateTime
case class SteamCortellis(user_id: String, customer_name: String)
case class SnowplowEventsCortellis(appsessionid: String, sid: String, timestamp: Long, encoding: String, collector: String, useragent: String, refereruri: String, path: String, headers: String, hostname: String, schema: String, tna: String, aid: String, p: String, dtm: Long, stm: Long, tz: String, e: String, eid: String, tv: String, nuid: String, tnuid: String, vid: String, ip: String, res: String, dvce_screenwidth: Integer, dvce_screenheight: Integer, url: String, ua: String, page: String, refr: String, fp: String, ctype: String, cookie: String, lang: String, f_pdf: Integer, f_qt: Integer, f_realp: Integer, f_wma: Integer, f_dir: Integer, f_fla: Integer, f_java: Integer, f_gears: Integer, f_ag: Integer, cd: String, ds: String, doc_width: Integer, doc_height: Integer, cs: String, vp: String, br_viewwidth: Integer, br_viewheight: Integer, mac: String, pp_mix: String, pp_max: String, pp_miy: String, pp_may: String, ad_ba: String, ad_ca: String, ad_ad: String, ad_uid: String, se_ca: String, se_ac: String, se_la: String, se_pr: String, se_va: String, co: String, ggl_ga: String, navigationstart: Long, unloadeventstart: Long, unloadeventend: Long, redirectstart: Long, redirectend: Long, fetchstart: Long, domainlookupstart: Long, domainlookupend: Long, connectstart: Long, connectend: Long, secureconnectionstart: Long, requeststart: Long, responsestart: Long, responseend: Long, domloading: Long, dominteractive: Long, domcontentloadedeventstart: Long, domcontentloadedeventend: Long, domcomplete: Long, loadeventstart: Long, loadeventend: Long, chromefirstpaint: Long, exptrack: String, customerid: String, product: String, searchtype: String, language: String, institution: String, alerttype: String, alertid: String, emailinstid: String, uri_redirect: String, sortBy: String, geo_state: String, geo_statecode: String, geo_metrocode: Integer, geo_areacode: Integer, geo_postalcode: String, traffic_source: String, overall_is_returing_user: Boolean, overall_first_login_date: Long, app_is_returing_user: Boolean, app_first_login_date: Long, page_from_url: String, uid_original: String, sid_original: String, uid: String, ipaddress: String, geo_city: String, geo_countrycode: String, geo_countryname: String, geo_latitude: Double, geo_longtitude: Double, browser: String, device: String, op_system: String, duid: String, networkuserid: String, ipaddress_original: String, geo_city_original: String, geo_countrycode_original: String, geo_countryname_original: String, geo_latitude_original: Double, geo_longtitude_original: Double, browser_original: String, device_original: String, op_system_original: String, duid_original: String, networkuserid_original: String, steamid: String, truid: String, email: String, customer_name: String, internal_user: String, pv_sequence: Integer, se_sequence: Integer, prior_alternate_e: Integer, subs_alternate_e: Integer, session_length_in_secs: Double, referrer_domain: String, referrer_path: String, dt: String)
case class SnowPlowEventsCortellis_SteamCortellis(customer_name: String, appsessionid: String, sid: String, timestamp: Long, encoding: String, collector: String, useragent: String, refereruri: String, path: String, headers: String, hostname: String, schema: String, tna: String, aid: String, p: String, dtm: Long, stm: Long, tz: String, e: String, eid: String, tv: String, nuid: String, tnuid: String, vid: String, ip: String, res: String, dvce_screenwidth: Integer, dvce_screenheight: Integer, url: String, ua: String, page: String, refr: String, fp: String, ctype: String, cookie: String, lang: String, f_pdf: Integer, f_qt: Integer, f_realp: Integer, f_wma: Integer, f_dir: Integer, f_fla: Integer, f_java: Integer, f_gears: Integer, f_ag: Integer, cd: String, ds: String, doc_width: Integer, doc_height: Integer, cs: String, vp: String, br_viewwidth: Integer, br_viewheight: Integer, mac: String, pp_mix: String, pp_max: String, pp_miy: String, pp_may: String, ad_ba: String, ad_ca: String, ad_ad: String, ad_uid: String, se_ca: String, se_ac: String, se_la: String, se_pr: String, se_va: String, co: String, ggl_ga: String, navigationstart: Long, unloadeventstart: Long, unloadeventend: Long, redirectstart: Long, redirectend: Long, fetchstart: Long, domainlookupstart: Long, domainlookupend: Long, connectstart: Long, connectend: Long, secureconnectionstart: Long, requeststart: Long, responsestart: Long, responseend: Long, domloading: Long, dominteractive: Long, domcontentloadedeventstart: Long, domcontentloadedeventend: Long, domcomplete: Long, loadeventstart: Long, loadeventend: Long, chromefirstpaint: Long, exptrack: String, customerid: String, product: String, searchtype: String, language: String, institution: String, alerttype: String, alertid: String, emailinstid: String, uri_redirect: String, sortBy: String, geo_state: String, geo_statecode: String, geo_metrocode: Integer, geo_areacode: Integer, geo_postalcode: String, traffic_source: String, overall_is_returing_user: Boolean, overall_first_login_date: Long, app_is_returing_user: Boolean, app_first_login_date: Long, page_from_url: String, uid_original: String, sid_original: String, uid: String, ipaddress: String, geo_city: String, geo_countrycode: String, geo_countryname: String, geo_latitude: Double, geo_Longtitude: Double, browser: String, device: String, op_system: String, duid: String, networkuserid: String, ipaddress_original: String, geo_city_original: String, geo_countrycode_original: String, geo_countryname_original: String, geo_latitude_original: Double, geo_Longtitude_original: Double, browser_original: String, device_original: String, op_system_original: String, duid_original: String, networkuserid_original: String, steamid: String, truid: String, email: String, internal_user: String, pv_sequence: Integer, se_sequence: Integer, prior_alternate_e: Integer, subs_alternate_e: Integer, session_length_in_secs: Double, referrer_domain: String, referrer_path: String, dt: String)

object CortellisEnrichment {

  def enrichment(spark: SparkSession, inputTable: String, startDate: String, endDate: String, enrichedTable: String, lastSteamCortellisDate: String) = {

    import spark.implicits._

    val reorderedColumnNames: Array[String] = Array("customer_name", "appsessionid", "sid", "timestamp", "encoding", "collector", "useragent", "refereruri", "path", "headers", "hostname", "schema", "tna", "aid", "p", "dtm", "stm", "tz", "e", "eid", "tv", "nuid", "tnuid", "vid", "ip", "res", "dvce_screenwidth", "dvce_screenheight", "url", "ua", "page", "refr", "fp", "ctype", "cookie", "lang", "f_pdf", "f_qt", "f_realp", "f_wma", "f_dir", "f_fla", "f_java", "f_gears", "f_ag", "cd", "ds", "doc_width", "doc_height", "cs", "vp", "br_viewwidth", "br_viewheight", "mac", "pp_mix", "pp_max", "pp_miy", "pp_may", "ad_ba", "ad_ca", "ad_ad", "ad_uid", "se_ca", "se_ac", "se_la", "se_pr", "se_va", "co", "ggl_ga", "navigationstart", "unloadeventstart", "unloadeventend", "redirectstart", "redirectend", "fetchstart", "domainlookupstart", "domainlookupend", "connectstart", "connectend", "secureconnectionstart", "requeststart", "responsestart", "responseend", "domloading", "dominteractive", "domcontentloadedeventstart", "domcontentloadedeventend", "domcomplete", "loadeventstart", "loadeventend", "chromefirstpaint", "exptrack", "customerid", "product", "searchtype", "language", "institution", "alerttype", "alertid", "emailinstid", "uri_redirect", "sortBy", "geo_state", "geo_statecode", "geo_metrocode", "geo_areacode", "geo_postalcode", "traffic_source", "overall_is_returing_user", "overall_first_login_date", "app_is_returing_user", "app_first_login_date", "page_from_url", "uid_original", "sid_original", "uid", "ipaddress", "geo_city", "geo_countrycode", "geo_countryname", "geo_latitude", "geo_longtitude", "browser", "device", "op_system", "duid", "networkuserid", "ipaddress_original", "geo_city_original", "geo_countrycode_original", "geo_countryname_original", "geo_latitude_original", "geo_longtitude_original", "browser_original", "device_original", "op_system_original", "duid_original", "networkuserid_original", "steamid", "truid", "email", "internal_user", "pv_sequence", "se_sequence", "prior_alternate_e", "subs_alternate_e", "session_length_in_secs", "referrer_domain", "referrer_path", "dt")

    // for good case scenario steams Cortellis data of currentDate has previous days data , hecne plus(1)
    if (lastSteamCortellisDate == null) {
      val SteamCortellisDateToRead = new DateTime(DateTime.parse(endDate)).plusDays(1).toString("YYYYMMdd")
      println("Reading Steams cortellis directory : " + SteamCortellisDateToRead)
      println("enrichment for good case scenario...")
      val SteamCortellisDateToReadS3 = "s3://1p-data-snowplow-prod/amit_test/cortellis/daily/" + SteamCortellisDateToRead
      val steamCortellis_extractDFStaging = spark.read.option("delimiter", "\t").csv(SteamCortellisDateToReadS3).distinct()
      val steamCortellis_extractDF = steamCortellis_extractDFStaging.select(col("_c0").as("user_id"), col("_c9").as("customer_name")).filter(col("user_id").isNotNull).distinct().as[SteamCortellis]
      val snowplow_events_cortellisDF = spark.sql(s"select * from $inputTable where dt > '$startDate' and dt<='$endDate' and (aid ='lsc' or aid ='cortellis')").as[SnowplowEventsCortellis]

      val steamCoretellisSNowPlowEvent = snowplow_events_cortellisDF.join(steamCortellis_extractDF, col("uid").equalTo(col("user_id")), "leftouter").drop(snowplow_events_cortellisDF.col("customer_name")).drop("user_id").select(reorderedColumnNames.head, reorderedColumnNames.tail: _*).as[SnowPlowEventsCortellis_SteamCortellis]

      steamCoretellisSNowPlowEvent.printSchema()
      steamCoretellisSNowPlowEvent.select(col("dt")).show(35, false)
      //steamCoretellisSNowPlowEvent.write.mode(org.apache.spark.sql.SaveMode.Overwrite).insertInto(s"$enrichedTable")
      steamCoretellisSNowPlowEvent.write.mode(SaveMode.Overwrite).insertInto(s"$enrichedTable")

    } else {
      val SteamCortellisLastDateToRead = new DateTime(DateTime.parse(lastSteamCortellisDate)).toString("YYYYMMdd")
      println("Reading Steams cortellis directory : " + SteamCortellisLastDateToRead)
      println("enrichment for bad case scenario...")
      val SteamCortellisDateToReadS3 = "s3://1p-data-snowplow-prod/amit_test/cortellis/daily/" + SteamCortellisLastDateToRead
      val steamCortellis_extractDFStaging = spark.read.option("delimiter", "\t").csv(SteamCortellisDateToReadS3).distinct()
      val steamCortellis_extractDF = steamCortellis_extractDFStaging.select(col("_c0").as("user_id"), col("_c9").as("customer_name")).filter(col("user_id").isNotNull).distinct().as[SteamCortellis]
      val snowplow_events_cortellisDF = spark.sql(s"select * from $inputTable where dt > '$startDate' and dt<='$endDate' and (aid ='lsc' or aid ='cortellis')").as[SnowplowEventsCortellis]

      val steamCoretellisSNowPlowEvent = snowplow_events_cortellisDF.join(steamCortellis_extractDF, col("uid").equalTo(col("user_id")), "leftouter").drop(snowplow_events_cortellisDF.col("customer_name")).drop("user_id").select(reorderedColumnNames.head, reorderedColumnNames.tail: _*).as[SnowPlowEventsCortellis_SteamCortellis]

      //steamCoretellisSNowPlowEvent.write.mode(org.apache.spark.sql.SaveMode.Overwrite).insertInto(s"$enrichedTable")
      steamCoretellisSNowPlowEvent.write.mode(SaveMode.Overwrite).insertInto(s"$enrichedTable")

    }

  }

  def main(args: Array[String]) {

    val spark = SparkSession.builder().appName("Cortellis Erichment Job").enableHiveSupport().getOrCreate()

    val topenv = args(0) //prod
    val subenv = args(1) //prod1
    val inputTable = args(2) //snowplow_events
    val database = "neon_" + topenv + "_" + subenv //neon_prod_prod1

    val enrichedTable = args(3) // snowplow_events_cortellis  // default.snowplow_events_cortellis_partitioned  // use this for final testing : default.snowplow_events_cortellis
    var startDate = args(4) //prevDay
    var endDate = args(5) //currDay

    spark.sql("set hive.exec.dynamic.partition.mode=nonstrict")
    spark.sql(s"USE $database")
    spark.sql(s"msck repair table $inputTable ")

    // When prod ready then uncomment below
    /*
    val ecrichBucket ="1p-data-snowplow-prod"
    val enrichPrefix = "enrich/prod-prod1/snowplow-cortellis"
    val steamBucket  = "steam-user-data"
    val staemPrefix = "cortellis/daily"*/

    // when prod ready comment below
    val ecrichBucket = "1p-data-snowplow-prod"
    val enrichPrefix = "amit_test/cortellis/enrichment"
    val steamBucket = "1p-data-snowplow-prod"
    val staemPrefix = "amit_test/cortellis/daily"

    val (missedDates, lastSteamCortellisDate) = Util.BackFillDates(ecrichBucket, enrichPrefix, steamBucket, staemPrefix)

    println("Missed dates for enrichment are : " + missedDates)
    println("Last date for Steam Cortellis date is : " + lastSteamCortellisDate)
    if (missedDates.size > 0) {
      for (Date <- missedDates) {
        println("Processing for date : " + Date)
        startDate = new DateTime(DateTime.parse(Date)).minusDays(1).toString("YYYY-MM-dd")
        endDate = Date
        println("...............Good case scenario.........")
        println("Enrichment for cortellis begins for below dates")
        println("Missed date is : " + Date)
        println("Start Date : " + startDate)
        println("End Date :" + endDate)
        enrichment(spark, inputTable, startDate, endDate, enrichedTable, null)
      }
    } else {
      println("..................Bad case scenario......")
      println("Missed dates for enrichment are : " + missedDates)
      println("Last date for Steam Cortellis date is : " + lastSteamCortellisDate)
      println("Start Date : " + startDate)
      println("End Date :" + endDate)
      enrichment(spark, inputTable, startDate, endDate, enrichedTable, lastSteamCortellisDate) //missedDates.value
    }

  }

}